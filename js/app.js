// 1) функція в програмуванні дозволяє написати частину коду з певним алгоритмом і звертатись до нього
// кожен раз коли потрібно повторити алгоритм дій, просто викликаючи цю функцію
// 2) аргумент - це значення, яке передається при виклику функції. Якщо наприклад, два параметри були задані
// у функції а при виклику задати лише один аргумент, то буде аргумент і undefined
// 3) return завершує виконання функції і повертає її значення


function getNum(numberName) {
    let num;
    while (!num || (num.length > 1 && num[0] === '0') || isNaN(+num)) {
        num = prompt(`Enter ${numberName} number`);
    }
    return +num;
}

function getOperator() {
    const availableOperators = ['+', '-', '*', '/'];
    let operator;
    while (!operator || !availableOperators.includes(operator)) {
        operator = prompt('Enter +, -, /, or *');
    }
    return operator;
}

function calcNumb() {
    const a = getNum('first');
    const b = getNum('second');
    const operator = getOperator();
    let result = a;
    switch (operator) {
        case '+':
            result += b;
            break;
        case '-':
            result -= b;
            break;
        case '*':
            result *= b;
            break;
        case '/':
            result /= b;
            break;
    }
    alert(result);
}

calcNumb();